module.exports = function(grunt) 
{
    require('load-grunt-tasks')(grunt); 
    
 
grunt.initConfig({
    //Компилятор SASS//
	sass: {
    dist: {
      files: {
        'build/css/style.css': 'source/sass/style.scss',
      }
    }
  },
    imagemin: {                          // Task 
   
    dynamic: {                         // Another target 
      files: [{
        expand: true,                  // Enable dynamic expansion 
        cwd: 'source/img/',                   // Src matches are relative to this path 
        src: ['**/*.{png,jpg,gif,svg,ico}'],   // Actual patterns to match 
        dest: 'build/img/'                  // Destination path prefix 
      }]
    }
  },
    copy: {
  files: {
    cwd: 'source/js',  // set working folder / root to copy
    src: '**/*',           // copy all files and subfolders
    dest: 'build/js',    // destination folder
    expand: true           // required when using cwd
  }
},
postcss: {
    options: {
        map: {
          inline: false, // save all sourcemaps as separate files...
          annotation: 'dist/css/maps/' // ...to the specified directory
        },
      processors: [
        require('pixrem')(), // add fallbacks for rem units
        require('autoprefixer')({browsers: 'last 2 versions'}), // add vendor prefixes
        require('cssnano')() // minify the result
      ]
    },
    dist: {
      src: 'build/css/style.css'
    }
  }
});
 
grunt.registerTask("build", [
"sass",
"imagemin",
"copy",
"postcss"
]);
}
